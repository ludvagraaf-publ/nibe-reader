-- DB structure for manual/remote creating
-- SQL commands for creating objects in target nibedata Schema

/* CREATE USER nibeusr;

CREATE DATABASE nibedata
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

GRANT TEMPORARY, CONNECT ON DATABASE nibedata TO PUBLIC;

GRANT ALL ON DATABASE nibedata TO nibeusr;

GRANT ALL ON DATABASE nibedata TO postgres;
*/
CREATE SEQUENCE IF NOT EXISTS public.rawdata_idrec_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.rawdata_idrec_seq
    OWNER TO nibeusr;

CREATE TABLE IF NOT EXISTS public.rawdata
(
    idrec integer NOT NULL DEFAULT nextval('rawdata_idrec_seq'::regclass),
    fetchtime timestamp without time zone NOT NULL DEFAULT now(),
    avg_outdoor_temp_bt1 numeric(4,1),
    blocked boolean,
    c_blocked boolean,
    c_charge_pump_speed_eb101 integer,
    c_compressor_operating_time_eb101 integer,
    c_compressor_operating_time_hot_water_eb101 integer,
    c_compressor_run_time_cooling_eb101 integer,
    c_compressor_starts_eb101 integer,
    c_condenser_out_eb101_bt12 numeric(4,1),
    c_evaporator_eb101_bt16 numeric(4,1),
    c_hot_gas_eb101_bt14 numeric(4,1),
    c_liquid_line_eb101_bt15 numeric(4,1),
    c_outdoor_temp_eb101_bt28 numeric(4,1),
    c_return_temp_eb101_bt3 numeric(4,1),
    c_suction_gas_eb101_bt17 numeric(4,1),
    calculated_flow_temp_s1 numeric(4,1),
    current_be1 numeric(4,1),
    current_be2 numeric(4,1),
    current_be3 numeric(4,1),
    degree_minutes integer,
    external_flow_temp_bt25 numeric(4,1),
    external_return_temp_bt71 numeric(4,1),
    flow_bf1 numeric(4,1),
    hot_water_charging_bt6 numeric(4,1),
    hot_water_top_bt7 numeric(4,1),
    max_step integer,
    outdoor_temp_bt1 numeric(4,1),
    status integer,
    room_temperature_bt50 numeric(4,1),
    CONSTRAINT rawdata_pkey PRIMARY KEY (idrec)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.rawdata OWNER to nibeusr;

#!/usr/bin/env bash
#
# Installer for NIBE Reader
#

shopt -s extglob
shopt -s dotglob

if ! pip3 list|grep -i virtualenv;
then
    sudo apt install python3-pip
    pip3 install --upgrade pip
    pip3 install virtualenv
fi
if [ ! -d .venv ];
then
    virtualenv -p python3 .venv
    source .venv/bin/activate
    pip3 install -r requirements.txt
else
    source .venv/bin/activate
fi


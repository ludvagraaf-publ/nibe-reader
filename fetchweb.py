from readconf import web_config, db_config
import requests
import re
from bs4 import BeautifulSoup


def nibe_parser(col_name, col_val):
    """ Parsing data from web. Units, etc. Provide row (name, value) for parsing """

    col_name = col_name.replace(".", "")
    col_name = col_name.replace(",", "")
    col_name = col_name.replace(" ", "_")
    col_name = col_name.replace("-", "_")
    col_val = col_val.replace("DM", "")
    col_val = col_val.replace("yes", "1")
    col_val = col_val.replace("no", "0")
    col_val = col_val.replace("on", "1")
    col_val = col_val.replace("off", "0")
    clean_value = re.match('(-?[0-9.]+)', col_val)
    if clean_value:
        value = clean_value.group(1)
    else:
        value = ''

    return col_name, value


NIBE_SYSTEM = web_config()
NIBE_DB = db_config()
# print(NIBE_SYSTEM)
# print(NIBE_DB)
URL = NIBE_SYSTEM['nibeweb']
PASSWD = NIBE_SYSTEM['pwd']
EMAIL = NIBE_SYSTEM['email']
URL_LANG = "{}/Language/en-GB".format(URL)
URL_LOGIN = "{}/LogIn".format(URL)
URL_STATUS = "{}/System/{}/Status/ServiceInfo".format(URL, NIBE_SYSTEM['system_id'])
URL_STATUS2 = "{}{}".format(URL_STATUS, "/1")


def main():
    # preparing environment (LANG, CREDS, STATUS)
    s = requests.session()
    r = s.post(URL_LOGIN, data={"Password": PASSWD, "Email": EMAIL})
    r.raise_for_status()
    r = s.post(URL_LANG)
    r.raise_for_status()
    r = s.get(URL_STATUS)
    r.raise_for_status()
    # time is not needed as DB handles that with '...DEFAULT now().."
    # or 'import time' needed
    # fetch_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    #
    # fetching data from HTML
    data = {}
    nb_web_page = r.text
    soup = BeautifulSoup(nb_web_page, "html.parser")
    tables = soup.find_all(name="tbody")
    for table in tables:
        rows = table.find_all('tr')
        for row in rows:
            line = row.find_all("td")
            col_name = line[0].text.strip().lower()
            col_value = line[1].text.strip()
            # arranging function
            header, value = nibe_parser(col_name, col_value)
            data[header] = value
    # does 2nd webpage exists?
    try:
        r = s.get(URL_STATUS2)
        r.raise_for_status()
        nb_web_page = r.text
        soup = BeautifulSoup(nb_web_page, "html.parser")
        tables = soup.find_all(name="tbody")
        for table in tables:
            rows = table.find_all('tr')
            for row in rows:
                line = row.find_all("td")
                col_name = "c_" + line[0].text.strip().lower()
                col_value = line[1].text.strip()
                # arranging function
                header, value = nibe_parser(col_name, col_value)
                data[header] = value
    except requests.exceptions.HTTPError:
        pass

    # print(data)
    return data


if __name__ == "__main__":
    main()

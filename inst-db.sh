#!/usr/bin/env bash
#
# Installer PostgreSQL for NIBE Reader
# nibe.cfg needs to be in the same directory (autogenerating not supported yet)
#

usage() {
    echo " Usage: $0 -h | -u <username> "
    echo "     -h    - this usage"
    echo "     -u    - local PSQL superuser role username (usually 'postgres'),"
    echo "           if Postgres is not yet installed, use 'postgres' anyway."
    echo " Ex: $0 -u postgres"
}

[ -z $1 ] || [ "$#" -lt 2 ] || [ "$1" != "-u"  ] && { usage;exit 1; }
[ ! -f "nibe.cfg" ] && { echo "nibe.cfg is missing!!";usage;exit 1; }

# DB preparation schema, structure 
prepareDB() {
    sudo su - -c "createuser $nibedbuser" $psqluser;
    sudo su - -c "psql -c \"ALTER USER nibeusr WITH PASSWORD '$newpwd';\"" $psqluser; 
    sudo su - -c "createdb nibedata" $psqluser;
    sudo su - -c "psql -c \"grant all privileges on database nibedata to nibeusr;\"" $psqluser;
    sudo su - -c "psql -d nibedata -c \"CREATE SEQUENCE IF NOT EXISTS public.rawdata_idrec_seq \\
         INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;\"" $psqluser;
    sudo su - -c "psql -d nibedata -c \"ALTER SEQUENCE public.rawdata_idrec_seq OWNER TO nibeusr; \"" $psqluser;
    sudo su - -c "psql -d nibedata -c \"CREATE TABLE IF NOT EXISTS public.rawdata \\
    ( idrec integer NOT NULL DEFAULT nextval('public.rawdata_idrec_seq'::regclass), \\
    fetchtime timestamp without time zone NOT NULL DEFAULT now(), \\
    avg_outdoor_temp_bt1 decimal(4,1), blocked boolean, c_blocked boolean, \\
    c_charge_pump_speed_eb101 integer, c_compressor_operating_time_eb101 integer, \\
    c_compressor_operating_time_hot_water_eb101 integer, \\
    c_compressor_run_time_cooling_eb101 integer, c_compressor_starts_eb101 integer, \\
    c_condenser_out_eb101_bt12 decimal(4,1), c_evaporator_eb101_bt16 decimal(4,1), \\
    c_hot_gas_eb101_bt14 decimal(4,1), c_liquid_line_eb101_bt15 decimal(4,1), \\
    c_outdoor_temp_eb101_bt28 decimal(4,1), c_return_temp_eb101_bt3 decimal(4,1), \\
    c_suction_gas_eb101_bt17 decimal(4,1), calculated_flow_temp_s1 decimal(4,1), \\
    current_be1 decimal(4,1), current_be2 decimal(4,1), current_be3 decimal(4,1), \\
    degree_minutes integer, external_flow_temp_bt25 decimal(4,1), \\
    external_return_temp_bt71 decimal(4,1), flow_bf1 decimal(4,1), hot_water_charging_bt6 decimal(4,1), \\
    hot_water_top_bt7 decimal(4,1), max_step integer, outdoor_temp_bt1 decimal(4,1), \\
    status integer, room_temperature_bt50 decimal(4,1), CONSTRAINT rawdata_pkey PRIMARY KEY (idrec) ) \\
    TABLESPACE pg_default;\"" $psqluser;
    sudo su - -c "psql -d nibedata -c \"ALTER TABLE IF EXISTS public.rawdata OWNER to nibeusr;\"" $psqluser;
}

# install Postgres
instDB() {
    sudo apt update
    sudo apt install postgresql
    sudo systemctl restart postgresql
}
shopt -s extglob
shopt -s dotglob

psqluser=$2
pgpassfile=$(awk -F= '/passfile=/ { print $2 }' nibe.cfg)
eval pgpassfile=$pgpassfile
if [ ! -f $pgpassfile ]; then
    chars=abcdefghijklmnopqrstuv12345678ABCD
    for i in {1..8} ; do
        newpwd=$newpwd${chars:RANDOM%${#chars}:1}
    done
    echo "localhost:5432:nibedata:nibeusr:$newpwd" >> $pgpassfile
    echo "Created ~/.pgpass file automatically." #$(cat $pgpassfile)
else
    newpwd=$(awk -F: '{ print $5 }' $pgpassfile)
fi 
nibedbuser=$(awk -F: '{ print $4 }' $pgpassfile)
#echo $pgpassfile
#echo $nibedbuser
#if [ -z "$nibedbuser" ]; then
#    nibedbuser="nibeusr"
#    echo "nibbbbbusr: $nibedbuser"
#fi 

# is PGSQL aready installed and accessible?
echo "Checking PGSQL..."
echo "Superuser credentials needed."
psqlver=$(sudo su - -c "psql -V;" $psqluser)
if [ ! -z "$psqlver" ];then
    echo "PGSQL already installed"
    nibedb=$(sudo su - -c "psql -d nibedata -c \"SELECT * FROM rawdata;\"" $psqluser) 
    if [ ! -z $nibedb ];then
        echo "DB already exists!"
        exit 1
    else
        echo "nibe DB structure not found, preparing"
        prepareDB
    fi
else
    echo "PostgreSQL not installed, insalling. Provide root password if asked."
    instDB
    sleep 1
    prepareDB
fi
echo "Done."


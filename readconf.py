# Lets read configuration

import os
from configparser import ConfigParser
import re


def web_config(filename='nibe.cfg', section='NibeUplink'):
    parser = ConfigParser()
    parser.read(filename)
    web = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            web[param[0]] = param[1]
        web_cred = web['nibefile']
        data = []
        try:
            with open(os.path.expanduser(web_cred), 'r') as f:
                for line in f:
                    for cred in line.strip().split(":"):
                        data.append(cred)
                web['system_id'] = data[0]
                web['email'] = data[1]
                web['pwd'] = data[2]
        except OSError as e:
            print(f"{type(e)}: {e}")
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return web


def db_config(filename='nibe.cfg', section='pgsql'):
    parser = ConfigParser()
    parser.read(filename)
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = str(param[1].strip()).replace("\n", " ")
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db

# nibe-reader

Handy utility set which allows to store all historical data from [NIBE](https://nibeuplink.com/)
heat pumps to PostgreSQL database.
You need only basic subscription from NIBE (which is usually covered in standard installation of
heat pump itself). Read only web access for monitoring your system. 

***

## Installation
These bits are tested on Ubuntu 22.04 LTS, PostgreSQL 14 and Python 3.10. There are two main parts.
Database and web. You should start in *'nibe.cfg'* file with configuration. For DB part you can
stick with Default values as *'inst-db.sh'* script will handle new connection & save random password
for DB user. So far installation supports only local DB. However you can connect to remote server
and prepare DB structure manually from *'nibedata.sql'* file. The web monitor part really needs 
specific credentials from [NIBE](https://nibeuplink.com/) portal. 

**Prepare DB:**
```bash
./inst-db.sh -u postgres
```
**Install web-monitor:**
```bash
./inst-nibe.sh
```

## Usage
There is periodical pooling action in '/etc/crontab' file, running each 3 minutes. This will 
feed DB from web.

```bash
*/3 * *  * * root /full/pat/to/project/cronfetch.sh > /dev/null
```

## Support
Well yes, it's open source project - feel free to participate! 

## Roadmap
It started from need to keep useful data, when NIBE doesn't store them. 
For future plans the graphs sounds good and there is even some plays with matplotlib - there is still 
long journey to get final shape. Plans are big, time is limited. So if you want some additional
stuff, please drop us the message and we can prioritize our effort. So far tested with SMO-40 and should 
work with F2030/2040 pump.  
WIP - Nov 5. 2023 

## Contributing
So if you feel you can add something useful here, any help is appreciated. 

## License
This utility is open source project, except for NIBE part which is not included here. It's up to anyone
who wants to use free/paid version [NIBE Uplink](https://nibeuplink.com/).

## Project status
I did this in my free time, however this could be endless effort which is bigger than one-man-show. 
 